//
//  UINavigationController+Extension.swift
//  SideMenuExample
//
//  Created by kukushi on 25/02/2018.
//  Copyright © 2018 kukushi. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    open override var childForStatusBarHidden: UIViewController? {
        return self.topViewController
    }

    open override var childForStatusBarStyle: UIViewController? {
        return self.topViewController
    }    
    
    
}

extension UINavigationController {
    
    func popToViewControllerOfType(classForCoder: AnyClass) {
        for controller in viewControllers {
            if controller.classForCoder == classForCoder {
                popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func popViewControllers(controllersToPop: Int, animated: Bool) {
        if viewControllers.count > controllersToPop {
            popToViewController(viewControllers[viewControllers.count - (controllersToPop + 1)], animated: animated)
        } else {
            print("Trying to pop \(controllersToPop) view controllers but navigation controller contains only \(viewControllers.count) controllers in stack")
        }
    }
//    func addNavBarImage(leftBarItem: (String?, UIImage?, Selector?)? = nil, rightBarItem: (String?, UIImage?, Selector?)? = nil) {
//        
//        let navController = navigationController!
//        
//        let image = UIImage(named: "fusion-logo.png") //Your logo url here
//        let imageView = UIImageView(image: image)
//        
//        let bannerWidth = navController.navigationBar.frame.size.width
//        let bannerHeight = navController.navigationBar.frame.size.height
//        
//        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
//        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
//        
//        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
//        imageView.contentMode = .scaleAspectFit
//        
//        navigationItem.titleView = imageView
//        
//        if let leftBarItem = leftBarItem {
//            let (title, image, leftBarSelector) = leftBarItem
//            navigationItem.leftBarButtonItem = getBarButtonItem(withTitle: title, image: image, andSelector: leftBarSelector)
//        }
//        
//        if let rightBarItem = rightBarItem {
//            let (title, image, rightBarSelctor) = rightBarItem
//            navigationItem.rightBarButtonItem = getBarButtonItem(withTitle: title, image: image, andSelector: rightBarSelctor)
//        }
//    }
}
@IBDesignable extension UINavigationController {
    @IBInspectable var barTintColor: UIColor? {
        set {
            navigationBar.barTintColor = newValue
        }
        get {
            guard  let color = navigationBar.barTintColor else { return nil }
            return color
        }
    }
    
    @IBInspectable var tintColor: UIColor? {
        set {
            navigationBar.tintColor = newValue
        }
        get {
            guard  let color = navigationBar.tintColor else { return nil }
            return color
        }
    }
    
}
extension UINavigationController {
    
    func removeViewController(_ controller: UIViewController.Type) {
        if let viewController = viewControllers.first(where: { $0.isKind(of: controller.self) }) {
            viewController.removeFromParent()
        }
    }
}
