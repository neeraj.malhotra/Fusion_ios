//
//  UIViewController+NavigationBar.swift
//  B2CScheduler
//
//  Created by Sagar Kumar on 21/01/19.
//  Copyright © 2019 Bafflesol. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func configureNavigationBarTitleColor(withTitle title: String = "",color:UIColor, leftBarItem: (String?, UIImage?, Selector?)? = nil, rightBarItem: (String?, UIImage?, Selector?)? = nil, shouldShowBackground: Bool = true) {
        navigationController?.navigationBar.isHidden = false
        navigationItem.hidesBackButton = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        if let leftBarItem = leftBarItem {
            let (title, image, leftBarSelector) = leftBarItem
            navigationItem.leftBarButtonItem = getBarButtonItem(withTitle: title, image: image, andSelector: leftBarSelector)
        }
        
        if let rightBarItem = rightBarItem {
            let (title, image, rightBarSelctor) = rightBarItem
            navigationItem.rightBarButtonItem = getBarButtonItem(withTitle: title, image: image, andSelector: rightBarSelctor)
        }
        
        
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            self.navigationController?.navigationBar.isTranslucent = true  // pass "true" for fixing iOS 15.0 black bg issue
            self.navigationController?.navigationBar.tintColor = UIColor.white // We need to set tintcolor for iOS 15.0
            appearance.shadowColor = .clear    //removing navigationbar 1 px bottom border.
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        }else{
            navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.barTintColor = UIColor.clear
            
        }
        self.navigationItem.title = title
        
        let textAttributes = [NSAttributedString.Key.foregroundColor:color,NSAttributedString.Key.font: UIFont(name: "Calibri-Bold", size: 25)!]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        
    }
    
    
    func getBarButtonItem(withTitle title: String? = nil, image buttonImage: UIImage? = nil, andSelector selector: Selector?) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.setTitle(title, for: .normal)
        button.setTitleColor(B2C.Color.appThemeColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(buttonImage, for: .normal)
        if let selector = selector {
            button.addTarget(self, action: selector, for: .touchUpInside)
        }
        
        return UIBarButtonItem(customView: button)
    }
    
    func addNavBarWithImage(image navImage: UIImage? = nil,leftBarItem: (String?, UIImage?, Selector?)? = nil, rightBarItem: (String?, UIImage?, Selector?)? = nil) {
        
        let navController = navigationController!
        
        let image = navImage //Your logo url here
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        
        let bannerX = bannerWidth / 2 - (image!.size.width) / 2
        let bannerY = bannerHeight / 2 - (image!.size.height) / 2
        
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit
        
        navigationItem.titleView = imageView
        
        if let leftBarItem = leftBarItem {
            let (title, image, leftBarSelector) = leftBarItem
            navigationItem.leftBarButtonItem = getBarButtonItem(withTitle: title, image: image, andSelector: leftBarSelector)
        }
        
        if let rightBarItem = rightBarItem {
            let (title, image, rightBarSelctor) = rightBarItem
            navigationItem.rightBarButtonItem = getBarButtonItem(withTitle: title, image: image, andSelector: rightBarSelctor)
        }
    }
}
extension UINavigationBar {
    func setupNavigationBar() {
        let titleImageWidth = frame.size.width * 0.32
        let titleImageHeight = frame.size.height * 0.64
        var navigationBarIconimageView = UIImageView()
        if #available(iOS 11.0, *) {
            navigationBarIconimageView.widthAnchor.constraint(equalToConstant: titleImageWidth).isActive = true
            navigationBarIconimageView.heightAnchor.constraint(equalToConstant: titleImageHeight).isActive = true
        } else {
            navigationBarIconimageView = UIImageView(frame: CGRect(x: 0, y: 0, width: titleImageWidth, height: titleImageHeight))
        }
        navigationBarIconimageView.contentMode = .scaleAspectFit
        navigationBarIconimageView.image = UIImage(named: "fusion-logo.png")
        topItem?.titleView = navigationBarIconimageView
                
        
    }
    
    
    
//    func setupNavigationBar() {
//
//        //let titleImageWidth = frame.size.width * 0.32
//        //let titleImageHeight = frame.size.height * 0.64
//        let titleImageWidth = frame.size.width
//        let titleImageHeight = frame.size.height
//        let image = UIImage(named: "fusion-logo.png") //Your logo url here
//
//        let bannerX = titleImageWidth / 2 - (image?.size.width)! / 2
//        let bannerY = titleImageHeight / 2 - (image?.size.height)! / 2
//
//        var navigationBarIconimageView = UIImageView()
//        if #available(iOS 11.0, *) {
//            navigationBarIconimageView.widthAnchor.constraint(equalToConstant: titleImageWidth).isActive = true
//            navigationBarIconimageView.heightAnchor.constraint(equalToConstant: titleImageHeight).isActive = true
//        } else {
//            navigationBarIconimageView = UIImageView(frame: CGRect(x: bannerX, y: bannerY, width: titleImageWidth, height: titleImageHeight))
//        }
//        navigationBarIconimageView.contentMode = .scaleAspectFit
//        navigationBarIconimageView.image = image
//        topItem?.titleView = navigationBarIconimageView
//    }
}


//extension UINavigationController {
//
//    func popToViewControllerOfType(classForCoder: AnyClass) {
//        for controller in viewControllers {
//            if controller.classForCoder == classForCoder {
//                popToViewController(controller, animated: true)
//                break
//            }
//        }
//    }
//
//    func popViewControllers(controllersToPop: Int, animated: Bool) {
//        if viewControllers.count > controllersToPop {
//            popToViewController(viewControllers[viewControllers.count - (controllersToPop + 1)], animated: animated)
//        } else {
//            print("Trying to pop \(controllersToPop) view controllers but navigation controller contains only \(viewControllers.count) controllers in stack")
//        }
//    }
//    func addNavBarImage(leftBarItem: (String?, UIImage?, Selector?)? = nil, rightBarItem: (String?, UIImage?, Selector?)? = nil, shouldShowBackground: Bool = true) {
//
//        let navController = navigationController!
//
//        let image = UIImage(named: "fusion-logo.png") //Your logo url here
//        let imageView = UIImageView(image: image)
//
//        let bannerWidth = navController.navigationBar.frame.size.width
//        let bannerHeight = navController.navigationBar.frame.size.height
//
//        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
//        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
//
//        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
//        imageView.contentMode = .scaleAspectFit
//
//        navigationItem.titleView = imageView
//
//        if let leftBarItem = leftBarItem {
//            let (title, image, leftBarSelector) = leftBarItem
//            navigationItem.leftBarButtonItem = getBarButtonItem(withTitle: title, image: image, andSelector: leftBarSelector)
//        }
//
//        if let rightBarItem = rightBarItem {
//            let (title, image, rightBarSelctor) = rightBarItem
//            navigationItem.rightBarButtonItem = getBarButtonItem(withTitle: title, image: image, andSelector: rightBarSelctor)
//        }
//    }
//}
