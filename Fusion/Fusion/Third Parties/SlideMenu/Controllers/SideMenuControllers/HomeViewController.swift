//
//  HomeViewController.swift
//  SideMenu-IOS-Swift
//
//  Created by apple on 12/01/22.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var sideMenuBtn: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        sideMenuBtn.target = revealViewController()
        sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
        self.addNavBarWithImage(image: B2C.Image.fusion, leftBarItem: nil, rightBarItem: (nil, B2C.Image.chat,  #selector(leftBarButtonAction)))


    }
    @objc func leftBarButtonAction() {
        view.endEditing(true)
        //navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       super.viewWillDisappear(animated)

   }
    

}
