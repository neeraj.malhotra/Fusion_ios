//
//  Font.swift
//  B2CScheduler
//
//  Created by Sagar Kumar on 21/01/19.
//  Copyright © 2019 Bafflesol. All rights reserved.
//

import Foundation
import UIKit

typealias B2CFont = UIFont

extension B2CFont {
    
    static func regular(withFontSize size: CGFloat) -> B2CFont {
        return B2CFont(name: B2C.Font.regular, size: size)!
    }
    
    static func light(withFontSize size: CGFloat) -> B2CFont {
        return B2CFont(name: B2C.Font.regular, size: size)!
    }
    
    static func bold(withFontSize size: CGFloat) -> B2CFont {
        return B2CFont(name: B2C.Font.bold, size: size)!
    }
}
