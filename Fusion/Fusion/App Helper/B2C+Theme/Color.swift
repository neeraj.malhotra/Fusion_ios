//
//  Color.swift
//  B2CScheduler
//
//  Created by Sagar Kumar on 21/01/19.
//  Copyright © 2019 Bafflesol. All rights reserved.
//#colorLiteral(red: 0.2941176471, green: 0.6039215686, blue: 0.8901960784, alpha: 1)

import Foundation
import UIKit

extension B2C {
    
    struct Color {
        static let appThemeColor = #colorLiteral(red: 0.2980181873, green: 0.4107741117, blue: 0.4323282838, alpha: 1)
        static let light = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
        static let yellow = #colorLiteral(red: 0.9294117647, green: 0.6549019608, blue: 0.01176470588, alpha: 1)
        static let lightGray = #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1)
        static let green = #colorLiteral(red: 0.04705882353, green: 0.7215686275, blue: 0.04705882353, alpha: 1)
        static let newAppThemeColor = #colorLiteral(red: 1, green: 0.1764705882, blue: 0.3333333333, alpha: 1)
        static let placeholderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        static let defaultLabelColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        static let gray = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    }
}
