//
//  B2CScheduler.swift
//  B2CScheduler
//
//  Created by Sagar Kumar on 21/01/19.
//  Copyright © 2019 Bafflesol. All rights reserved.
//

import Foundation
import UIKit

enum B2C {
    
    struct FontSize {
        static let small = CGFloat(14.0)
        static let regular = CGFloat(16.0)
        static let large = CGFloat(18.0)
        static let veryLarge = CGFloat(20.0)
    }
    
    struct Font {
        static let regular = "Calibri"
        static let bold = "Calibri-Bold"
        static let light = ""
    }
}
