//
//  Image.swift
//  B2CScheduler
//
//  Created by Sagar Kumar on 21/01/19.
//  Copyright © 2019 Bafflesol. All rights reserved.
//

import Foundation
import UIKit

typealias Image = B2C.Image

extension B2C {
    
    struct Image {
        static let back = #imageLiteral(resourceName: "left-arrow")
        static let menu = #imageLiteral(resourceName: "menu")
        static let cross = #imageLiteral(resourceName: "multiply")
        static let edit = #imageLiteral(resourceName: "edit")
        static let menuWhite = #imageLiteral(resourceName: "menuWhite")
        static let brifcase = #imageLiteral(resourceName: "briefcase")
        static let tick = #imageLiteral(resourceName: "tick")
        static let share = #imageLiteral(resourceName: "share")
        static let more = #imageLiteral(resourceName: "editMore")
        static let phonebook = #imageLiteral(resourceName: "phonebook")
        static let calendarRemove = #imageLiteral(resourceName: "calendarRemove")
        static let search = #imageLiteral(resourceName: "search")
        static let chat = #imageLiteral(resourceName: "chat")
        static let fusion = #imageLiteral(resourceName: "fusion-logo")
        static let totalHealth = #imageLiteral(resourceName: "totalHealth")

    }
}
