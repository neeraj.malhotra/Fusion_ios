//
//  LoginViewController.swift
//  Fusion
//
//  Created by Neeraj Malhotra on 28/04/22.
//

import UIKit

class LoginVC: UIViewController {
    @IBOutlet weak var viewBackground: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        //configureNavigationBarTitleColor(withTitle: Navigation.totalhealth, color: B2C.Color.appThemeColor)
        self.addNavBarWithImage(image: B2C.Image.totalHealth, leftBarItem: nil, rightBarItem: nil)

        viewBackground.roundCorners([.bottomLeft, .bottomRight], radius: 50)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func login(_ sender: UIButton) {
        
        let story = UIStoryboard(name: "SideMenu", bundle: nil)
        let home = story.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.navigationController?.pushViewController(home, animated: true)
        
    }
    

}
