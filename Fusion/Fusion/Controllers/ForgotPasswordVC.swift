//
//  ForgotPasswordVC.swift
//  Fusion
//
//  Created by Neeraj Malhotra on 28/04/22.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var viewBackground: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true

        self.addNavBarWithImage(image: B2C.Image.totalHealth, leftBarItem: nil, rightBarItem: nil)
        viewBackground.roundCorners([.bottomLeft, .bottomRight], radius: 50)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backToLogin(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
