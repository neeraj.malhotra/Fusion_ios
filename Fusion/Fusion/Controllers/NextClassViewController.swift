//
//  NextClassViewController.swift
//  Fusion
//
//  Created by Neeraj Malhotra on 11/05/22.
//

import UIKit

class NextClassViewController: UIViewController {

    @IBOutlet weak var sideMenuBtn: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideMenuBtn.target = revealViewController()
        sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
        
       // self.navigationController?.addNavBarImage(leftBarItem: nil, rightBarItem: (B2C.Image.chat.imageWithColor(color: B2C.Color.white), #selector(leftBarButtonAction))?, shouldShowBackground: false)
        
        self.addNavBarWithImage(image: B2C.Image.fusion, leftBarItem: nil, rightBarItem: (nil, B2C.Image.chat,  #selector(leftBarButtonAction)))

        //addNavBarImage()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
       super.viewWillDisappear(animated)
        navigationController?.removeViewController(NextClassViewController.self)

   }
    
   
    
    @objc func leftBarButtonAction() {
        view.endEditing(true)
        //navigationController?.popViewController(animated: true)
    }
    
    
//    func addNavBarImage() {
//
//            let navController = navigationController!
//
//            let image = UIImage(named: "fusion-logo.png") //Your logo url here
//            let imageView = UIImageView(image: image)
//
//            let bannerWidth = navController.navigationBar.frame.size.width
//            let bannerHeight = navController.navigationBar.frame.size.height
//
//            let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
//            let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
//
//            imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
//            imageView.contentMode = .scaleAspectFit
//
//            navigationItem.titleView = imageView
//        }


}
//extension UINavigationBar {
////    func setupNavigationBar() {
////
////        //let titleImageWidth = frame.size.width * 0.32
////        //let titleImageHeight = frame.size.height * 0.64
////        let titleImageWidth = frame.size.width
////        let titleImageHeight = frame.size.height
////        let image = UIImage(named: "fusion-logo.png") //Your logo url here
////
////        let bannerX = titleImageWidth / 2 - (image?.size.width)! / 2
////        let bannerY = titleImageHeight / 2 - (image?.size.height)! / 2
////
////        var navigationBarIconimageView = UIImageView()
////        if #available(iOS 11.0, *) {
////            navigationBarIconimageView.widthAnchor.constraint(equalToConstant: titleImageWidth).isActive = true
////            navigationBarIconimageView.heightAnchor.constraint(equalToConstant: titleImageHeight).isActive = true
////        } else {
////            navigationBarIconimageView = UIImageView(frame: CGRect(x: bannerX, y: bannerY, width: titleImageWidth, height: titleImageHeight))
////        }
////        navigationBarIconimageView.contentMode = .scaleAspectFit
////        navigationBarIconimageView.image = image
////        topItem?.titleView = navigationBarIconimageView
////    }
//}
